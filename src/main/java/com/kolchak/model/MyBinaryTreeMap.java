package com.kolchak.model;

import sun.management.jmxremote.LocalRMIServerSocketFactory;

import java.util.*;

public class MyBinaryTreeMap<K extends Comparable, V> implements Map<K, V> {
    private int size;
    private Node<K, V> root;

    private static class Node<K, V> implements Map.Entry<K, V> {
        private Node<K, V> left;
        private Node<K, V> right;
        private K key;
        private V value;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }

//        private Node addRecursive(Node current, V value) {
//            if (current == null){
//                return new Node(value);
//            }
//            if (value < current.value) {
//                current.left = addRecursive(current.left, value);
//            } else if (value > current.value) {
//                current.right = addRecursive(current.right, value);
//            }
//            return current;

//        }

    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0 ? true : false;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public V get(Object key) {
        Comparable<? super K> k = (Comparable<? super K>) key;
        Node<K, V> node = root;
        while (node != null) {
            int cmp = k.compareTo(node.key);
            if (cmp < 0) {
                node = node.left;
            } else if (cmp > 0) {
                node = node.right;
            } else return node.value;
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        Node<K, V> parent = root;
        Node<K, V> tempParent = root;
        if (root == null) {
            root = new Node<>(key, value);
        } else {
            int cmp;
            do {
                parent = tempParent;
                if (cmp < 0) {
                    tempParent = tempParent.left;
                } else if (cmp > 0) {
                    tempParent = tempParent.right;
                } else {
                    V oldValue = tempParent.value;
                    tempParent.value = value;
                    return oldValue;
                }
            } while (tempParent != null);
            if (cmp > 0) {
                parent.left = new Node<>(key, value);
            } else if (cmp > 0) {
                parent.right = new Node<>(key, value);
            }
        }
        size++;
        return null;
    }

    @Override
    public V remove(Object key) {
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        Set<K> set = new HashSet<>();
        runTreeForNode(root, set);
        return set;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> set = new HashSet<>();
        runTreeForNode(root, set);
        return set;
    }

    private void runTreeForNode(Node<K, V> localRoot, Set<Entry<K, V>> set) {
        if (localRoot != null) {
            runTreeForNode(localRoot.left, set);
            set.add(localRoot);
            runTreeForNode(localRoot.right, set);
        }
    }

    private void runTreeForKey(Node<K, V> localRoot, Set<K> set) {
        if (localRoot != null) {
            runTreeForKey(localRoot.left, set);
            set.add(localRoot.getKey());
            runTreeForKey(localRoot.right.set);
        }
    }
}